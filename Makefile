build:
	pip install --upgrade pip setuptools wheel
	python setup.py sdist bdist_wheel
	rm -Rf build

install:
	pip install --upgrade pip setuptools wheel
	pip install -e ".[test]"

tests:
	nosetests versioneer/tests/