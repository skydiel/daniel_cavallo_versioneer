from setuptools import setup, find_packages


setup(
    name='versioneer',
    version='0.1',
    url='https://gitlab.com/skydiel/ormuco_ai_ml.git',
    license='MIT',
    author='Daniel Cavallo',
    author_email='skydiel@yahoo.com.br',
    description='Provides utilities to deal with strings containing versions.',
    packages=find_packages(exclude=['versioneer/tests']),
    long_description=open('README.md').read(),
    zip_safe=False,
    extras_require={
        'test': [
            'nose',
            'parameterized',
        ]
    }
)
