import re

_PATTERN = r'^\d+(\.\d+){0,2}$'


def compare(version1, version2):
    """Compare two strings containing versions and determine if one is greater, equal or less than the other.

    :param version1: a number/string containing a version in the format MAJOR[.MINOR[.PATCH]]
    :param version2: a number/string containing a version in the format MAJOR[.MINOR[.PATCH]]

    :return: -1 if version1 is less than version2, 0 if they are equal or 1 if version1 is greater than version2
    """
    # if not (isinstance(version1, str) and isinstance(version2, str)):
    #     raise ValueError('compare accepts only string parameters')
    _v1, _v2 = str(version1), str(version2)

    if not (re.search(_PATTERN, _v1) and re.search(_PATTERN, _v2)):
        raise ValueError('versions must in format MAJOR[.MINOR[.PATCH]]')

    if _v1 == _v2:
        return 0

    return -1 if _v1 < _v2 else 1
