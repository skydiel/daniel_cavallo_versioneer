from parameterized import parameterized
from unittest import TestCase

import versioneer


class TestCompareModule(TestCase):

    @parameterized.expand([
        ('1', '2', -1),
        ('1', '1', 0),
        ('2', '1', 1),
        ('0.7', '0.8', -1),
        ('1.3', '1.3', 0),
        ('1.4', '1.3', 1),
        ('1.4.3', '1.5.0', -1),
        ('1.1.1', '1.1.1', 0),
        ('2.1.1', '1.1.1', 1),
        ('0.0.0', '0.1', -1),
        ('1.3', '1.2.9', 1),
        ('1.9.9', '2', -1),
        (1, 2, -1),
        ('1', 2, -1),
        (2, '1', 1),
        (2, '2', 0),
        (1.0, '2', -1),
        (2.1, '2.0', 1),
    ])
    def test_compare(self, v1, v2, expected):

        self.assertEqual(versioneer.compare(v1, v2), expected)

    @parameterized.expand([
        ('1.0', True),
        (['2.0'], '0.1'),
        ({'2.0'}, '0.1'),
        ('2.0', {'version': '0.1'}),
        (('2.0',), '2.3'),
    ])
    def test_compare_with_invalid_parameters_raise_exception(self, v1, v2):

        with self.assertRaises(ValueError):
            versioneer.compare(v1, v2)
